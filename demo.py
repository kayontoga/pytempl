#!/usr/bin/env python3
#
# -*- coding: utf-8 -*-
#
# Time-stamp: <Saturday 2021-05-01 09:11:06 AEST Graham Williams>
#
# Copyright (c) Togaware Pty Ltd. All rights reserved.
# Licensed under GPLv3.
# Author: Graham.Williams@togaware.com
#
# A script to demonstrate ...
#
# ml demo pytempl

# This is mostly example code that may be useful. It is not suggested
# that every bit of code is useful in every script.

# -----------------------------------------------------------------------
# Load the required packages.
# -----------------------------------------------------------------------

import sys
import wget

from mlhub.pkg import mlcat, mlask

# -----------------------------------------------------------------------
# Check if we are running Python3.
# -----------------------------------------------------------------------

if (sys.version_info < (3, 0)):
    print("MLHub requires python3. Exiting...")
    sys.exit(1)

mlcat("Hello World", """\
Use this as a template and test of mlhub.ai for Python based tools.

Introduce the package and technology here with some introductory text for
the user to read whilst libraries get loaded.
""")

# Then pause here to allow the user to finish reading.

mlask(end="\n")

# -----------------------------------------------------------------------
# Get started.
# -----------------------------------------------------------------------

mlcat("Now Get Into the Details", f"""\
Start explaining what is happening.

In this simple example here we print the version of a dependency.

Package wget is version {wget.__version__}.
""")

mlask(end="\n")
