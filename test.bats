#!/usr/bin/bats

@test "test.py default" {
    run python3 test.py
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "Tested" ]
}
